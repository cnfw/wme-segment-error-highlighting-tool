import { observer } from "mobx-react";
import React from "react";

import MarkerClusterGroup from "react-leaflet-markercluster";

import { Map, Marker, Popup, TileLayer } from "react-leaflet";
import { stores } from "../stores";

const WalMap = observer(() => {
  let segmentStore = stores.csvStore;

  return (
    <div className="map">
      <Map center={{ lat: 53.6, lng: -7 }} zoom={7}>
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <MarkerClusterGroup chunkedLoading={true}>
          {segmentStore.filteredData.map((segment, i) => (
            <Marker position={[segment.Latitude, segment.Longitude]} key={i}>
              <Popup>Name: {segment.Name}</Popup>
            </Marker>
          ))}
        </MarkerClusterGroup>
      </Map>
    </div>
  );
});

export default WalMap;
