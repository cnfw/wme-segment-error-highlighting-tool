import React from "react";
import { Marker } from "react-leaflet";

type ErrorMarkerProps = {
  position: [number, number];
};

const ErrorMarker = ({ position }: ErrorMarkerProps) => {
  return <Marker position={position} />;
};

export default ErrorMarker;
