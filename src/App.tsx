import React from "react";
import Sidebar from "./Sidebar/Sidebar";
import WalMap from "./WalMap/WalMap";
import { observer } from "mobx-react";
import { StoreContext, stores } from "./stores";

const App: React.FC = observer(() => {
  return (
    <StoreContext.Provider value={stores}>
      <header>
        <h2>WME Segment Error Highlighting Tool</h2>
      </header>
      <section>
        <Sidebar />
        <WalMap />
      </section>
    </StoreContext.Provider>
  );
});

export default App;
