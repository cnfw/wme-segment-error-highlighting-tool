import React from "react";

import { observer } from "mobx-react";

import File from "./File";
import Info from "./Info";
import Options from "./Options/Options";

const Sidebar: React.FC = observer(() => {
  return (
    <div className="sidebar">
      <File />
      <Info />

      <h3>Options</h3>
      <Options />

      <div style={{ position: "absolute", bottom: "5px" }}>
        <small>v0.0.1 - Built for Waze UK and Ireland</small>
      </div>
    </div>
  );
});

export default Sidebar;
