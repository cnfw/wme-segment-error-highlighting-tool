import React from "react";

import { useStores } from "../stores";
import { observer } from "mobx-react";

const Info: React.FC = observer(() => {
  const { csvStore } = useStores();

  let dataPoints = csvStore.data.length;

  return (
    <>
      <h3>Info</h3>
      <ul>
        <li>
          <strong>Data points loaded: </strong>
          {dataPoints}
        </li>
      </ul>
    </>
  );
});

export default Info;
