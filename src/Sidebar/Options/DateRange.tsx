import React from "react";
import { observer } from "mobx-react";
import { useStores } from "../../stores";

const DateRange: React.FC = observer(() => {
  const { csvStore } = useStores();

  return (
    <>
      <h4>
        Date Range{" "}
        <small>
          {new Date(csvStore.lowerDate).toLocaleDateString()} -{" "}
          {new Date(csvStore.upperDate).toLocaleDateString()}
        </small>
      </h4>
      {csvStore.data.length === 0 && (
        <p>
          <em>Load a file first</em>
        </p>
      )}
    </>
  );
});

export default DateRange;
