import { createContext, useContext } from "react";

import SegmentStore from "./SegmentStore";

export const stores = {
  csvStore: new SegmentStore(),
};

export const StoreContext = createContext(stores);

export const useStores = () => {
  return useContext(StoreContext);
};
