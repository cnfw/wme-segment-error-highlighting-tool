import { observable, action, computed } from "mobx";
import Segment from "../interfaces/Segment";

const numericalColumns: (keyof Segment)[] = [
  "Lock Level",
  "Last Edited",
  "Latitude",
  "Longitude",
];

const stringColumns: (keyof Segment)[] = [
  "Name",
  "Alt Names",
  "Road Type",
  "Last Editor",
];

const columnsToRead: (keyof Segment)[] = [
  ...numericalColumns,
  ...stringColumns,
];

// TODO move out to own store and make configurable
const validAbbreviations = [
  "Ave",
  "Blvd",
  "Bdwy",
  "Cir",
  "Cl",
  "Ct",
  "Cr",
  "Dr",
  "E",
  "Gdn",
  "Gdns",
  "Gn",
  "Gr",
  "Ln",
  "Mt",
  "N",
  "Pl",
  "Pk",
  "Rdg",
  "Rd",
  "St.",
  "S",
  "Sq",
  "St",
  "Ter",
  "W",
];

const endingWhitelist = [
  "Lodge",
  "Ferry",
  "View",
  "Manor",
  "Brae",
  "Braes",
  "Mews",
  "Meadows",
  "Grange",
  "Hill",
  "Walk",
  "Rise",
];

const roadTypesWithNoNamePermitted = ["Parking Lot Road", "Private Road"];

export default class SegmentStore {
  @observable data: Segment[] = [];

  @computed get filteredData() {
    return this.data.filter((segment) => {
      return segment.Name.includes(" Cres");
      if (!segment.Name.includes(" ")) {
        // Names with no spaces are fine and don't need shown usually
        return false;
      }

      if (
        segment.Name === "No street" &&
        roadTypesWithNoNamePermitted.includes(segment["Road Type"])
      ) {
        // Don't flag segments as having no name if they are of a type that allows no names
        return false;
      }

      let nameParts = segment.Name.split(" ");

      // Catch problems with abbreviations and streets starting "The "
      if (nameParts.length === 2 && nameParts[0] === "The") {
        return false;
      }

      let endOfName = nameParts[nameParts.length - 1];

      if (nameParts.length > 1) {
        if (endingWhitelist.includes(endOfName)) {
          return false;
        }

        if (validAbbreviations.includes(endOfName)) {
          return false;
        }
      }

      return true;
    });
  }

  @action parseCsv(csvText: string) {
    let newData = [];

    let lines = csvText.split("\n");
    let headers = lines[0].split(",") as (keyof Segment)[];

    // i = 1, ignores the header line in the CSV
    for (let i = 1; i < lines.length; i++) {
      let thisLine = lines[i].split(",");
      let rowObj: any = {};

      for (let j = 0; j < headers.length; j++) {
        if (!columnsToRead.includes(headers[j])) {
          continue;
        }

        if (numericalColumns.includes(headers[j])) {
          // If a numerical column
          rowObj[headers[j]] = +thisLine[j];
        } else {
          // Otherwise its a string column
          rowObj[headers[j]] = thisLine[j].replace(/"/g, "");
        }
      }

      if (
        !(
          (!(rowObj.Longitude <= 0) && !(rowObj.Longitude > 0)) ||
          (!(rowObj.Latitude <= 0) && !(rowObj.Latitude > 0))
        )
      ) {
        newData.push(rowObj);
      }
    }

    this.data = newData;
  }
}
