export default interface Segment {
  Name: string;
  "Road Type": string;
  "Last Edited": number;
  "Alt Names": string;
  "Lock Level": number;
  "Last Editor": string;

  Latitude: number;
  Longitude: number;
}
