export const yearsAgo = (numYears = 1) => {
  let aYearAgo = new Date();
  aYearAgo.setFullYear(aYearAgo.getFullYear() - numYears);

  return aYearAgo.getTime();
};
